import AppLogin from './AppLogin';

import AppContent from './AppContent';

import ResetPasswordPage from './pages/AuthPages/ResetPasswordPage';

import DesignDashboard from './pages/DesignDashboard/DesignDashboard';
import DesignDashboardOrder from './pages/DesignDashboard/DesignDashboardOrder.vue';

import MaterialDashboard from './pages/MaterialDashboard/MaterialDashboard';
import MaterialDashboardQueuesList from './pages/MaterialDashboard/MaterialDashboardQueuesList';
import MaterialDashboardOneQueue from './pages/MaterialDashboard/MaterialDashboardOneQueue';

import ShipmentDashboard from './pages/ShipmentDashboard/ShipmentDashboard';
import ShipmentDashboardBoxPage from './pages/ShipmentDashboard/ShipmentDashboardBoxPage';
import ShipmentDashboardItemPage from './pages/ShipmentDashboard/ShipmentDashboardItemPage';
import ShipmentDashboardBocPackingPage from './pages/ShipmentDashboard/ShipmentDashboardBoxPackingPage';

import UserManagement from './pages/UserManagement/UserManagement';

export const routes = [

    { path: '/login', component: AppLogin },

    { path: '/reset/:hash', component: ResetPasswordPage },


    { path: '', component: AppContent, children: [
        { path: '', redirect: '/design'},
        { path: '/design', component: DesignDashboard },
        { path: '/design/order/:id', component: DesignDashboardOrder },
        
        { path: '/material', component: MaterialDashboard },
        { path: '/material/order/:id', component: DesignDashboardOrder },
        { path: '/material/queues', component: MaterialDashboardQueuesList },
        { path: '/material/queues/:id', component: MaterialDashboardOneQueue },
    
        { path: '/shipment', component: ShipmentDashboard },
        { path: '/shipment/box/:id', component: ShipmentDashboardBoxPage },
        { path: '/shipment/box/:id/item/:itemId', component: ShipmentDashboardItemPage },
        { path: '/shipment/packing', component: ShipmentDashboardBocPackingPage },

        { path: '/user-management', component: UserManagement }
    
    ]},

   
];