import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import { store } from './store/store'

// Vue Material Imports
import {
        MdButton, 
        MdTable,
        MdContent,
        MdIcon,
        MdTabs,
        MdDialog,
        MdCheckbox,
        MdList,
        MdApp,
        MdDrawer,
        MdToolbar,
        MdCard,
        MdLayout,
        MdField,
        MdMenu,
        MdTooltip,
        MdSnackbar
      } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'

// Custom Components
import AppInput from './components/Base/AppInput.vue'
import AppRadioInput from './components/Base/AppRadioInput.vue'
import AppContentHeader from './components/Base/AppContentHeader.vue'
import AppEmptyState from './components/Base/AppEmptyState.vue'

// Custom SCSS file
import './assets/scss/main.scss'

// Routes settings
import VueRouter from 'vue-router'
import { routes } from './router'

// Form Validation
import Vuelidate from 'vuelidate'

// Date Formating filter
import VueFilterDateFormat from 'vue-filter-date-format';

// QR codes reader
import VueQrcodeReader from "vue-qrcode-reader";


/** Hides production mode warnings */ 
Vue.config.productionTip = false

/** Form Validation */ 
Vue.use(Vuelidate);

/** Router settings */
Vue.use(VueRouter);
const router = new VueRouter({
  routes
});

/** Axios plugin settings */
Vue.use({
  install (Vue) {
    Vue.prototype.$axios = axios.create({
      baseURL: process.env.VUE_APP_API_BASE_URL,
    })
  }
});

/** Vue Material components */
Vue.use(MdButton);
Vue.use(MdTable);
Vue.use(MdContent);
Vue.use(MdIcon);
Vue.use(MdTabs);
Vue.use(MdDialog);
Vue.use(MdCheckbox);
Vue.use(MdList);
Vue.use(MdApp);
Vue.use(MdDrawer);
Vue.use(MdToolbar);
Vue.use(MdCard);
Vue.use(MdLayout);
Vue.use(MdField);
Vue.use(MdMenu);
Vue.use(MdTooltip);
Vue.use(MdSnackbar);


/** Custom components use */
Vue.component('AppInput', AppInput);
Vue.component('AppRadioInput', AppRadioInput);
Vue.component('AppContentHeader', AppContentHeader);
Vue.component('AppEmptyState', AppEmptyState);

/** Filters */
Vue.use(VueFilterDateFormat);

/** Qr codes reader */
Vue.use(VueQrcodeReader);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')