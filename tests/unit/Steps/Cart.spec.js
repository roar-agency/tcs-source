import { expect } from 'chai'
import { createLocalVue, mount } from '@vue/test-utils'
import Cart from '@/components/Steps/Cart/Cart'

import { MdButton, MdContent, MdIcon, MdList } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import Vuelidate from 'vuelidate'
import Vuex from 'vuex'

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuelidate);
localVue.use(MdButton);
localVue.use(MdContent);
localVue.use(MdIcon);
localVue.use(MdList);


let wrapper;
let store;
let actions;
let getters;


beforeEach(() => {

    actions = {
        dummyAction: () => null
    };

    getters = {
        getScreenWidth: () => null,
    };

    store = new Vuex.Store({
        actions,
        getters,
    });

    wrapper = mount(Cart,
        {
            sync: false,
            localVue,
            store
        });
});

afterEach(() => {
    wrapper.destroy();
});

describe('Cart.vue', () => {
    it('Submit payment button should be enabled', () => {
        const submitBtn = wrapper.find('.step-content__btn');
        expect(submitBtn.attributes('class')).to.contain('md-raised md-accent');
    });
});