import { expect } from 'chai'
import { createLocalVue, mount } from '@vue/test-utils'
import CompareAndShop from '@/components/Steps/CompareAndShop/CompareAndShop.vue'

import AppInput from '@/components/Base/AppInput.vue'
import AppRadioInput from '@/components/Base/AppRadioInput.vue'
import { MdButton, MdTable, MdContent, MdIcon, MdTabs, MdDialog, MdCheckbox, MdList } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import Vuelidate from 'vuelidate'
import Vuex from 'vuex'

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuelidate);
localVue.component('AppInput', AppInput);
localVue.component('AppRadioInput', AppRadioInput);
localVue.use(MdButton);
localVue.use(MdTable);
localVue.use(MdContent);
localVue.use(MdIcon);
localVue.use(MdTabs);
localVue.use(MdDialog);
localVue.use(MdCheckbox);
localVue.use(MdList);


let wrapper;
let store;
let actions;
let getters;


beforeEach(() => {

    actions = {
        actionClick: () => null,
        actionInput: () => null
    };

    getters = {
        getScreenWidth: () => null,
    };

    store = new Vuex.Store({
        actions,
        getters,
    });

    wrapper = mount(CompareAndShop,
        {
            sync: false,
            localVue,
            store
        });
});

afterEach(() => {
    wrapper.destroy();
});

describe('CompareAndShop.vue', () => {
    it('Submit button should be enabled', () => {
        const submitBtn = wrapper.find('.step-content__btn');
        expect(submitBtn.attributes('class')).to.contain('md-raised md-accent');
    });
});