import { expect } from 'chai'
import { createLocalVue, mount } from '@vue/test-utils'
import ApplicantFormMembersInfo from '@/components/Steps/MembersInfo/ApplicantFormMembersInfo';

import AppInput from '@/components/Base/AppInput.vue'
import AppRadioInput from '@/components/Base/AppRadioInput.vue'
import { MdButton, MdTable, MdContent, MdIcon, MdTabs, MdDialog, MdCheckbox, MdList } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import Vuelidate from 'vuelidate'
import Vuex from 'vuex'


const localVue = createLocalVue()

localVue.use(Vuex);
localVue.use(Vuelidate);
localVue.component('AppInput', AppInput);
localVue.component('AppRadioInput', AppRadioInput);
localVue.use(MdButton);
localVue.use(MdTable);
localVue.use(MdContent);
localVue.use(MdIcon);
localVue.use(MdTabs);
localVue.use(MdDialog);
localVue.use(MdCheckbox);
localVue.use(MdList);


let wrapper;
let store;
let actions;
let getters;


beforeEach(() => {
    actions = {
        actionClick: () => null,
        actionInput: () => null
    };
    getters = {
        getScreenWidth: () => null,
    };
    store = new Vuex.Store({
        actions,
        getters,
    });
    wrapper = mount(ApplicantFormMembersInfo,
        {
            sync: false,
            localVue,
            store
        })
    
});

afterEach(() => {
    wrapper.destroy();
});

describe('ApplicantFormMembersInfo.vue', () => {

    it('Submit button should be disabled if form is empty', () => {
        const submitBtn = wrapper.find('.step-content__btn');
        expect(submitBtn.attributes('class')).to.contain('step-content__btn--disabled');
    });

    
    it('Component should not display input errors, unless form was touched', async() => {
        const allErrors = wrapper.findAll('.control-field__error').length;
        expect(allErrors).to.equal(0);
    });

    
    it('Component should display input erros for each field, if form is empty', async() => {
        wrapper.vm.$v.mainMember.$touch();

        await wrapper.vm.$nextTick();

        const allErrors = wrapper.findAll('.control-field__error').length;
        const allValidators = Object.keys(wrapper.vm.$v.mainMember.$params).length;

        expect(allErrors).to.equal(allValidators);
    });

    
    it('Submit button should be enabled if form is filled out', async () => {

        wrapper.setData({
            mainMember: {
                firstName: 'John',
                lastName: 'Lorem',
                email: 'email@mail.com',
                phone: '123-1234-222',
                state: 'GA',
                city: 'London',
                address: 'Some street, 12',
                comments: 'Some comments'
            },
            productType: {
                validity: true
            }
        });

        wrapper.vm.$v.mainMember.$touch();

        await wrapper.vm.$nextTick();

        const submitBtn = wrapper.find('.step-content__btn');

        expect(wrapper.vm.mainMember.firstName).to.equal('John');
        expect(wrapper.vm.$v.mainMember.$invalid).to.equal(false);
        expect(submitBtn.attributes('class')).to.contain('md-raised md-accent');
    });


    it('SubmitStep method should be emitted when step-content button was clicked', async() => {

        wrapper.setData({
            mainMember: {
                firstName: 'John',
                lastName: 'Lorem',
                email: 'email@mail.com',
                phone: '123-1234-222',
                state: 'GA',
                city: 'London',
                address: 'Some street, 12',
                comments: 'Some comments'
            },
            productType: {
                validity: true
            }
        });

        const submitBtn = wrapper.find('.step-content__btn');        
        submitBtn.trigger('click');

        await wrapper.vm.$nextTick();
        expect(wrapper.emitted()).to.have.own.property('changeStep');
    });



});