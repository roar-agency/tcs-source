import { expect } from 'chai'
import { createLocalVue, mount } from '@vue/test-utils'
import ApplicantFormPrimaryInfo from '@/components/Steps/PrimaryInfo/ApplicantFormPrimaryInfo'

import AppInput from '@/components/Base/AppInput.vue'
import AppRadioInput from '@/components/Base/AppRadioInput.vue'
import { MdButton, MdTable, MdContent, MdIcon, MdTabs, MdDialog, MdCheckbox } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import Vuelidate from 'vuelidate'

const localVue = createLocalVue()

localVue.use(Vuelidate);
localVue.component('AppInput', AppInput);
localVue.component('AppRadioInput', AppRadioInput);
localVue.use(MdButton);
localVue.use(MdTable);
localVue.use(MdContent);
localVue.use(MdIcon);
localVue.use(MdTabs);
localVue.use(MdDialog);
localVue.use(MdCheckbox);

let wrapper;

beforeEach(() => {
    wrapper = mount(ApplicantFormPrimaryInfo,
        {
            sync: false,
            localVue
        });
});

afterEach(() => {
    wrapper.destroy();
});


describe('ApplicationFormPrimaryInfo.vue', () => {

    it('Submit button should be disabled if form is empty', () => {
        const submitBtn = wrapper.find('.step-content__btn');
        expect(submitBtn.attributes('class')).to.contain('step-content__btn--disabled');
    });

    
    it('Component should not display input errors, unless form was touched', async() => {
        const allErrors = wrapper.findAll('.control-field__error').length;
        expect(allErrors).to.equal(0);
    });

    
    it('Component should display input erros for each field, if form is empty', async() => {
        wrapper.vm.$v.primaryInfo.$touch();

        await wrapper.vm.$nextTick();

        const allErrors = wrapper.findAll('.control-field__error').length;
        const allValidators = Object.keys(wrapper.vm.$v.primaryInfo.$params).length;

        expect(allErrors).to.equal(allValidators);
    });

    
    it('Submit button should be enabled when form is filled out', async() => {
        
        wrapper.setData({
            primaryInfo: {
                zipCode: '12321',
                country: 'Ukraine',
                state: 'UK',
                people: '1',
                income: 'no',
                planType: '1'
            }
        });

        wrapper.vm.$v.primaryInfo.$touch();

        await wrapper.vm.$nextTick();

        const submitBtn = wrapper.find('.step-content__btn');
        expect(wrapper.vm.primaryInfo.zipCode).to.equal('12321');
        expect(wrapper.vm.$v.primaryInfo.$invalid).to.equal(false);
        expect(submitBtn.attributes('class')).to.contain('md-raised md-accent');
    });

    
    it('SubmitStep method should be emitted when step-content button was clicked', async() => {

        wrapper.setData({
            primaryInfo: {
                zipCode: '12321',
                country: 'Ukraine',
                state: 'UK',
                people: '1',
                income: 'no',
                planType: '1'
            }
        });

        const submitBtn = wrapper.find('.step-content__btn');        
        submitBtn.trigger('click');

        await wrapper.vm.$nextTick();
        expect(wrapper.emitted()).to.have.own.property('changeStep');
    });


});