import { expect } from 'chai'
import { createLocalVue, mount } from '@vue/test-utils'
import CompareAndShopFilters from '@/components/Steps/CompareAndShop/CompareAndShopFilters'

import { MdCheckbox } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'

const localVue = createLocalVue();

localVue.use(MdCheckbox);

let wrapper;

beforeEach(() => {
    wrapper = mount(CompareAndShopFilters,
        {
            sync: false,
            localVue,
            propsData: {
                filters: [
                    {
                        all: [
                            {
                                name: 'HMO',
                                quantity: 4
                            },
                            {
                                name: 'HMO2',
                                quantity: 5
                            }
                        ],
                        active: [],
                        name: 'Plan Type'
                    },
    
                    {
                        all: [
                            {
                                name: 'Gold',
                                quantity: 4
                            },
                            {
                                name: 'Silver',
                                quantity: 5
                            }
                        ],
                        active: [],
                        name: 'Metal Tier'
                    },
    
                    {
                        all: [
                            {
                                name: 'No Deductible',
                                quantity: 0
                            },
                            {
                                name: '$1 - 1000',
                                quantity: 5
                            },
                            {
                                name: '$1001 - 3,500',
                                quantity: 5
                            },
                            {
                                name: '$3,501 - 5,000',
                                quantity: 5
                            }
                        ],
                        active: [],
                        name: 'Deductible Range True'
                    },                
                ],
            }
        });
});

afterEach(() => {
    wrapper.destroy();
});

describe('CompareAndShop.vue', () => {
    it('All filters group and filters should be rendered properly', () => {
        const filtersGroupElements = wrapper.findAll('.filter-group').length;
        const filtersElements = wrapper.findAll('.filter-group_filter').length;
        
        const filterGroupsInProps = wrapper.props().filters.length;
        const filtersInProps = wrapper
                                .props()
                                .filters
                                .reduce((acc, x) => acc += x.all.length, 0);

        expect(filtersGroupElements).to.equal(filterGroupsInProps);
        expect(filtersElements).to.equal(filtersInProps);
    });
});