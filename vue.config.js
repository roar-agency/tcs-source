module.exports = {
    publicPath: './',
    configureWebpack: {
        output: {
            filename: 'index.bundle.js'
        },
        optimization: {
            splitChunks: false
        },
    },
    filenameHashing: false,
    css: {
        loaderOptions: {
            sass: {
                prependData: `
                    @import "@/assets/scss/abstracts/_variables.scss";
                    @import "@/assets/scss/abstracts/_mixins.scss";
                    `
            }
        }
    }
}